# **Prueba Técnica Colppy**
Este es un proyecto que tiene como finalidad automatizar el despliegue de una infraestructura en AWS con la utilización de Cloudformation.

## **Consigna**:
Crear una infraestructura la cual contenga una aplicación "Front-end" para un supuesto equipo de desarrollo. Este despliegue se espera que sea acorde a las buenas prácticas de la nube AWS.
Generar un despliegue de un Front-end sencillo el cual contenga un "Hola Nubox".
Se espera:
-    Despliegue como código (cloudFormation).
-    Implementación a través de CI/CD.
-    Infraestructura debe contener como mínimo EC2, ALB, AutoScaling.
-    Incluir README con detalles del despliegue y un diagrama descriptivo.

# Cómo usar

**Un nuevo commit en el repositorio disparará el pipeline. Este pipeline tiene tres stages:**

### 1. **CFN Lint**:
Realiza un linter sobre el archivo Colppy-stack.yaml, que contiene las configuraciones de la infraestructura a desplegar.

### 2. **Package**:
Construye y pushea a la registry del repositorio (Gitlab) la [imagen](https://gitlab.com/faku.alcatraz/prueba-tecnica-colppy/container_registry/2693327) del contenedor que cumple la función de web server y con el archivo index.html.

### 1. **Cloudformation Deploy**:
Ejecuta una serie de comandos para inciar sesión en AWS y despliega el stack de CloudFormation con el nombre "ColppyStack". Este stage utiliza las variables correspondientes de Gitlab para AWS.

> **_NOTA:_** No debe haber ningún stack con el mismo nombre ya desplegado o en estado de rollback. Todavía no está implementada la lógica para realizar **update** en vez de **deploy**

# Diagrama de Infraestructura

![Diagrama](Diagrama-Infraestructura.png?raw=true)

# Referencias

-   https://about.gitlab.com/blog/2020/12/15/deploy-aws/
-   https://www.udemy.com/course/cloudformation/
-   https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html
-   https://gitlab.com/gitlab-org/cloud-deploy/-/tree/master/aws
-   https://aws.amazon.com/blogs/mt/git-pre-commit-validation-of-aws-cloudformation-templates-with-cfn-lint/
-   https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

# Estado del proyecto
Finalizada la consigna. Abierto a mejoras.